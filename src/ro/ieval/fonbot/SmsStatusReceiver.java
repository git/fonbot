package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;
import org.eclipse.jdt.annotation.Nullable;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * BroadcastReceiver that receives SMS status broadcasts, notifying the user.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class SmsStatusReceiver extends BroadcastReceiver {
	/**
	 * Formats a SMS message intent into a human-readable string
	 *
	 * @param intent the SMS message
	 * @return the string representation of the SMS
	 */
	private static String describeSMS(final Intent intent){
		return "To: "+intent.getStringExtra(EXTRA_DESTINATION)+
				", Part: "+intent.getIntExtra(EXTRA_PART, 1)+" of "+intent.getIntExtra(EXTRA_TOTAL, 1);
	}

	/**
	 * Broadcast action: SMS sent
	 */
	public static final String SENT_ACTION="ro.ieval.fonbot.SmsStatusReceiver.SENT";
	/**
	 * Broadcast action: SMS delivered
	 */
	public static final String DELIVERED_ACTION="ro.ieval.fonbot.SmsStatusReceiver.DELIVERED";
	/**
	 * Extra: SMS destination (phone number)
	 */
	public static final String EXTRA_DESTINATION="destination";
	/**
	 * Extra: SMS part number
	 */
	public static final String EXTRA_PART="part";
	/**
	 * Extra: SMS part count
	 */
	public static final String EXTRA_TOTAL="total";
	/**
	 * Extra: Notification address
	 */
	public static final String EXTRA_REPLY_TO="reply_to";

	@Override
	public void onReceive(@Nullable final Context context, @Nullable final Intent intent) {
		if(intent==null||context==null)
			return;
		final Address replyTo=new Address(Utils.toNonNull(intent.getStringExtra(EXTRA_REPLY_TO)));
		if(intent.getAction().startsWith(SENT_ACTION))
			switch(getResultCode()){
			case Activity.RESULT_OK:
				Utils.sendConfirmMessage(context, replyTo, sms_sent, describeSMS(intent));
				break;
			case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				Utils.sendMessage(context, replyTo, sms_not_sent_generic_failure, describeSMS(intent));
				break;
			case SmsManager.RESULT_ERROR_NO_SERVICE:
				Utils.sendMessage(context, replyTo, sms_not_sent_no_service, describeSMS(intent));
				break;
			case SmsManager.RESULT_ERROR_NULL_PDU:
				Utils.sendMessage(context, replyTo, sms_not_sent_null_pdu, describeSMS(intent));
				break;
			case SmsManager.RESULT_ERROR_RADIO_OFF:
				Utils.sendMessage(context, replyTo, sms_not_sent_radio_off, describeSMS(intent));
				break;
			}
		else if(intent.getAction().startsWith(DELIVERED_ACTION))
			switch(getResultCode()){
			case Activity.RESULT_OK:
				Utils.sendMessage(context, replyTo, sms_delivered, describeSMS(intent));
				break;
			case Activity.RESULT_CANCELED:
				Utils.sendMessage(context, replyTo, sms_not_delivered, describeSMS(intent));
				break;
			}
	}
}
