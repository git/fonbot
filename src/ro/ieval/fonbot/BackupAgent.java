package ro.ieval.fonbot;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * Backup agent. Backups the preferences file
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class BackupAgent extends BackupAgentHelper {
	@Override
	public void onCreate() {
		addHelper("prefs", new SharedPreferencesBackupHelper(this,
				getClass().getPackage().getName()+"_preferences"));
	}
}
