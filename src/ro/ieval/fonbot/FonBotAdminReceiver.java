package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;

import static ro.ieval.fonbot.Utils.toNonNull;

import org.eclipse.jdt.annotation.Nullable;

import ro.ieval.fonbot.Utils.MessageType;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * The {@link DeviceAdminReceiver} used by FonBot. Sends {@link Utils.MessageType#ADMIN ADMIN} and {@link Utils.MessageType#WATCH_LOGIN WATCH_LOGIN} notifications. 
 * 
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class FonBotAdminReceiver extends DeviceAdminReceiver {
	@Override
	public void onDisabled(@Nullable final Context context, @Nullable final Intent intent) {
		if(context==null)
			return;
		Utils.sendMessage(context, toNonNull(MessageType.ADMIN), admin_disabled);
	}

	@Override
	public @Nullable CharSequence onDisableRequested(@Nullable final Context context, @Nullable final Intent intent) {
		if(context==null){
			Log.wtf(getClass().getName(), "context is null in onDisableRequested");
			throw new AssertionError("Log.wtf did not terminate the process");
		}
		Utils.sendMessage(context, toNonNull(MessageType.ADMIN), admin_disable_requested);
		return null;//TODO: write this
	}

	@Override
	public void onEnabled(@Nullable final Context context, @Nullable final Intent intent) {
		if(context==null)
			return;
		Utils.sendMessage(context, toNonNull(MessageType.ADMIN), admin_enabled);
	}

	@Override
	public void onPasswordChanged(@Nullable final Context context, @Nullable final Intent intent) {
		if(context==null)
			return;
		Utils.sendMessage(context, toNonNull(MessageType.ADMIN), device_password_changed);
	}

	@SuppressWarnings("boxing")
	@Override
	public void onPasswordFailed(@Nullable final Context context, @Nullable final Intent intent) {
		if(context==null)
			return;
		final DevicePolicyManager dpm=(DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
		Utils.sendMessage(context, toNonNull(MessageType.WATCH_LOGIN), device_login_failed_fmt, dpm.getCurrentFailedPasswordAttempts());
	}

	@Override
	public void onPasswordSucceeded(@Nullable final Context context, @Nullable final Intent intent) {
		if(context==null)
			return;
		Utils.sendMessage(context, toNonNull(MessageType.WATCH_LOGIN), device_login_succeeded);
	}
}
