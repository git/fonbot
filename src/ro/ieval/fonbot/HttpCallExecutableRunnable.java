package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;
import static ro.ieval.fonbot.Utils.toNonNull;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Collection;

import java.net.HttpURLConnection;
import org.eclipse.jdt.annotation.Nullable;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * ExecutableRunnable that makes a HTTP(S) call to the server and hands the response to a callback
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class HttpCallExecutableRunnable extends ExecutableRunnable{

	/**
	 * Callback which is run after a HTTP call.
	 *
	 * @author Marius Gavrilescu
	 */
	public interface ResultCallback{
		/**
		 * Callback invoked if the HTTP call is successful.
		 *
		 * @param responseCode HTTP response code
		 * @param responseMessage HTTP response message
		 * @param inputStream HTTP content InputStream
		 */
		void onResult(final int responseCode, final String responseMessage, final InputStream inputStream);
		/**
		 * Callback invoked if the HTTP call is unsuccessful.
		 *
		 * @param error localized error message
		 */
		void onError(final String error);
	}

	/**
	 * List of extra request headers.
	 */
	private final Collection<Header> headers;
	/**
	 * Context instance used by this class
	 */
	private final Context context;
	/**
	 * Data to send to the server
	 */
	private final byte[] data;
	/**
	 * Server URL path
	 */
	private final String path;
	/**
	 * Callback to run after the request returns
	 */
	private final ResultCallback callback;
	/** If true, the task should be retried if it fails */
	private final boolean mustRetryTask;

	/**
	 * Constructs a SendHttpMessageAsyncTask which sends a binary message.
	 *
	 * @param path URL path
	 * @param headers the extra headers
	 * @param context the context instance
	 * @param resultCallback {@link ResultCallback} instance
	 * @param mustRetryTask true if this task should be retried if it fails
	 * @param data the message to send
	 */
	public HttpCallExecutableRunnable(final String path, final @Nullable Collection<Header> headers, final Context context, final @Nullable ResultCallback resultCallback, final boolean mustRetryTask, final byte[] data){//NOPMD array is supposed to be immutable.
		this.path=path;
		this.headers=headers;
		this.context=context;
		this.callback=resultCallback;
		this.mustRetryTask=mustRetryTask;
		this.data=data;
	}

	/**
	 * Constructs a SendHttpMessageAsyncTask which sends a text message.
	 *
	 * @param path URL path
	 * @param headers the extra headers
	 * @param context the context instance
	 * @param resultCallback {@link ResultCallback} instance
	 * @param mustRetryTask true if this task should be retried if it fails
	 * @param message message to send
	 */
	public HttpCallExecutableRunnable(final String path, final @Nullable Collection<Header> headers, final Context context, final @Nullable ResultCallback resultCallback, final boolean mustRetryTask, final String... message){
		this.path=path;
		this.headers=headers;
		this.context=context;
		this.callback=resultCallback;
		this.mustRetryTask=mustRetryTask;
		if(message.length == 0)
			this.data=null;//NOPMD final field
		else
			this.data=Utils.join(" ", message).getBytes();
	}

	@Override
	public void run(){
		try {
			doRun();
		} catch (Exception e) {
			e.printStackTrace();
			if(callback != null)
				callback.onError(toNonNull(context.getString(connection_error)));
			if(mustRetryTask)
				retry();
		}
	}

	public void doRun() throws Exception{
		final URL url=Utils.getServerURL(toNonNull(context),toNonNull(path));
		final HttpURLConnection conn=(HttpURLConnection) url.openConnection();
		conn.setReadTimeout(24*60*1000);//24 minutes
		if(data!=null){
			conn.setDoOutput(true);
			conn.setFixedLengthStreamingMode(data.length);
		}
		final String user=PreferenceManager.getDefaultSharedPreferences(context).getString("username", null);
		final String password=PreferenceManager.getDefaultSharedPreferences(context).getString("password", null);
		if(user == null || password == null || user.length() == 0 || password.length() == 0){
			if(callback!=null)
				callback.onError(toNonNull(context.getString(user_or_password_not_set)));
			return;
		}

		conn.setRequestProperty("Authorization", "Basic "+Base64.encodeToString((user+':'+password).getBytes(), Base64.NO_WRAP));
		if(headers != null)
			for (final Header header : headers)
				conn.setRequestProperty(header.name, header.value);
		conn.connect();
		if(data!=null){
			final OutputStream stream=conn.getOutputStream();
			stream.write(data);
			stream.close();
		}
		Log.d(getClass().getName(),"HTTP Response: "+conn.getResponseCode()+" "+conn.getResponseMessage());
		String message=conn.getResponseMessage();
		if(message==null && callback != null)
			callback.onError(toNonNull(context.getString(no_response_returned_from_server)));
		else if(message != null && callback != null){
			if(message.charAt(message.length()-1) == ')')//message is (something)
				message=message.substring(1, message.length()-1);
			else if(message.charAt(0) == '(')//message is (something) something else
				message=message.substring(message.indexOf(')')+2);
			callback.onResult(conn.getResponseCode(), message, conn.getResponseCode() == 200 ? conn.getInputStream() : null);
		}
		conn.disconnect();
	}
}
