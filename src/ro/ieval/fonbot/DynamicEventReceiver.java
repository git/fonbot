package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;

import static ro.ieval.fonbot.Utils.toNonNull;

import org.eclipse.jdt.annotation.Nullable;

import ro.ieval.fonbot.Utils.MessageType;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * A BroadcastReceiver that receives "dynamic" events like {@link Intent#ACTION_SCREEN_ON ACTION_SCREEN_ON}, {@link Intent#ACTION_BATTERY_CHANGED ACTION_BATTERY_CHANGED} and {@link Intent#ACTION_HEADSET_PLUG} 
 * 
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class DynamicEventReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final @Nullable Context context, final @Nullable Intent intent) {
		if(context==null || intent==null || isInitialStickyBroadcast())
			return;

		final String action=intent.getAction();

		if(action.equals(Intent.ACTION_BATTERY_CHANGED))
			Heavy.describeBatteryLevel(context, null, toNonNull(MessageType.BATTERY_CHANGED));
		else if(action.equals(Intent.ACTION_HEADSET_PLUG)){
			final int state=intent.getIntExtra("state", 0);
			final String name=intent.getStringExtra("name");
			final int microphone=intent.getIntExtra("microphone", 0);

			final StringBuilder builder=new StringBuilder(100);
			if(microphone==1)
				builder.append(toNonNull(context.getString(headset_with_microphone)));
			else
				builder.append(toNonNull(context.getString(headset_without_microphone)));
			builder.append(' ');
			builder.append(name);
			builder.append(' ');
			if(state==1)
				builder.append(toNonNull(context.getString(plugged_in)));
			else
				builder.append(toNonNull(context.getString(unplugged)));
			builder.append(". ");

			Utils.sendMessage(context,
					toNonNull(MessageType.HEADSET),
					toNonNull(builder.toString()));
		}
	}

}
