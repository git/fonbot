package ro.ieval.fonbot;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.os.SystemClock;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * Slim alternative to AsyncTask.<p>
 *
 * Differences from AsyncTask:
 * <ul>
 * <li>No onPreExecute or onPostExecute
 * <li>No progress support
 * <li>Can be retried if necessary
 * </ul>
 * 
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
abstract class ExecutableRunnable implements Runnable {
	/** Executor used to execute instances of this class */
	private static final ScheduledExecutorService EXECUTORS = Executors.newSingleThreadScheduledExecutor();
	/** Queue containing <code>ExecutableRunnable</code>s that should be retried */
	private static final Queue<ExecutableRunnable> RETRY_PENDING_TASKS = new LinkedList<ExecutableRunnable>();
	/** Minimum interval between task retries, in milliseconds */
	private static final long RETRY_INTERVAL = 30000;
	/** {@link SystemClock#elapsedRealtime()} time of last successful call to {@link #retryTasks()} */
	private static long lastRetry = 0;
	/** True if a retryTasks run is already scheduled on {@link #EXECUTORS} */
	private static volatile boolean retryIsScheduled = false;

	/** Run all tasks that should be retried */
	public static final void retryTasks(){
		if(!retryIsScheduled && lastRetry+RETRY_INTERVAL>SystemClock.elapsedRealtime()){
			retryIsScheduled = true;
			EXECUTORS.schedule(new Runnable() {
				@Override
				public void run() {
					retryTasks();
					retryIsScheduled = false;
				}
			}, RETRY_INTERVAL, TimeUnit.MILLISECONDS);
			return;
		}
		synchronized(RETRY_PENDING_TASKS){
			for(ExecutableRunnable task : RETRY_PENDING_TASKS)
				EXECUTORS.execute(task);
			RETRY_PENDING_TASKS.clear();
		}
		lastRetry=SystemClock.elapsedRealtime();
	}

	/** Execute this <code>ExecutableRunnable</code> */
	public final void execute(){
		retryTasks();
		EXECUTORS.execute(this);
	}

	/** Mark this <code>ExecutableRunnable</code> as needing to be retried later */
	protected final void retry(){
		synchronized(RETRY_PENDING_TASKS){
			RETRY_PENDING_TASKS.add(this);
		}
	}
}
