package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import ro.ieval.fonbot.Address.Protocol;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * Utility functions and enums used in various places. 
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class Utils {
	/**
	 * Enum of all FonBot commands.
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	@SuppressWarnings("javadoc")
	public static enum Command{
		TOAST, ECHO, SMS, SMSQ, FLASH, WIFI,
		BLUETOOTH, DIAL, RING, SPEAK, VIBRATE,
		DIALOG, LOCATION, NOLOCATION, RINGER, NCFILE,
		PHOTO, SETNOTIFICATION, DELNOTIFICATION, SETPASSWORD, HELP,
		WIPE, LOCK, VIEW, PLAY, PAUSE,
		NEXT, PREV, BATT, CALLLOG, SMSLOG,
		LS, RM, CONTACTS, DISABLE, ENABLE,
		POLL, HANGUP, ANSWER, LAUNCH, DATA,
		GPS, GLOCATION, REBOOT, NOTIFY, SCREENCAP,
		TORCH, GETFILE, SH, ROOTSH
	}

	/**
	 * Enum of all message types. Each message type is a kind of notification which can be enabled/disabled by the user and directed to a certain address.
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 *
	 */
	public static enum MessageType{
		/** SMS notifications */
		SMS, 
		/** Phone state (idle, offhook or ringing) notifications */
		PHONE_STATE,
		/** Lockscreen failed password notifications */
		WATCH_LOGIN,
		/** Device admin enable/disable notifications */
		ADMIN,
		/** Coarse battery status (low battery, ok battery) notifications */
		BATTERY,
		/** Fine battery status notifications */
		BATTERY_CHANGED,
		/** Headset plug/unplug notifications */
		HEADSET,
		/** Phone booted notifications */
		BOOT,
	}

	/**
	 * Enum of wipe types. The two defined types are the data wipe (which does not include the SD card) and the full wipe (which includes the SD card).
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	public static enum WipeType{
		/** Factory reset the phone, without touching the SD card */
		DATA,
		/** Factory reset the phone and wipe the SD card too */
		FULL
	}

	/**
	 * Enum of ringer modes.
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	public static enum RingerMode{
		/** Sound is on */
		NORMAL,
		/** Sound is off, vibrate is on */
		VIBRATE,
		/** Sound is off, vibrate is off */
		SILENT
	}

	/**
	 * Enum of location providers
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	public static enum LocationProvider{
		/** The network location provider */
		NETWORK,
		/** The GPS location provider */
		GPS
	}

	/**
	 * Enum of toast lengths.
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	private static enum ToastLength{
		/** Long toast */
		LONG,
		/** Short toast */
		SHORT
	}

	/**
	 * Enum of the values on and off. Used for boolean arguments.
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	@SuppressWarnings("javadoc")
	private static enum OnOff{
		ON, OFF
	}

	/**
	 * Enum of ongoing event types
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	public static enum OngoingEvent{
		/** Location tracking is active. Registered by {@link Command#LOCATION}, unregistered by {@link Command#NOLOCATION} */
		LOCATION(location_tracking_is_active),
		/** The phone is ringing. Registered/unregistered by {@link Command#RING} */
		RING(ringing);

		/** String resource: the event description */
		public final int resource;

		/**
		 * Constructs an OngoingEvent from its event description.
		 *
		 * @param resource the event description
		 */
		private OngoingEvent(final int resource) {
			this.resource=resource;
		}
	}

	/** Confirmation string for the {@link Command#WIPE WIPE} command. */
	public static final String WIPE_CONFIRM_STRING="I am aware this cannot be undone";

	/**
	 * Converts a Nullable object into a NonNull one.
	 *
	 * @param object the Nullable object to convert
	 * @return a NonNull object equivalent to the Nullable parameter
	 * @throws AssertionError if the given object is null
	 */
	public static <T> T toNonNull(@Nullable T object) throws AssertionError{
		if(object==null)
			throw new NullPointerException();
		return object;
	}

	/**
	 * Join an array of Objects with elements separated by a separator into a single string.
	 *
	 * @param separator the separator
	 * @param offset the offset into the array
	 * @param args the array of Objects to join
	 * @return the joined string
	 */
	public static String join(final String separator,final int offset,final Object[] args){
		final StringBuilder sb=new StringBuilder(240);
		sb.append(args[offset]);
		for (int i = offset+1; i < args.length; i++) {
			sb.append(separator);
			sb.append(args[i]);
		}
		return toNonNull(sb.toString());
	}

	/**
	 * Join an array of Objects with elements separated by a separator into a single string.
	 *
	 * @param separator the separator
	 * @param args the array of objects to join
	 * @return the joined string
	 */
	public static String join(final String separator, final Object[] args){
		return join(separator,0,args);
	}

	/**
	 * Send a notification to the user.
	 *
	 * @param context Context instance
	 * @param type notification type
	 * @param resource String resource for the message text
	 */
	public static void sendMessage(final Context context, final MessageType type, final int resource){
		sendMessage(context, type, toNonNull(context.getString(resource)));
	}

	/**
	 * Send a notification to the user.
	 *
	 * @param context Context instance
	 * @param type notification type
	 * @param resource String resource for the message text
	 * @param args format parameters for the resource
	 */
	public static void sendMessage(final Context context, final MessageType type, final int resource, final Object... args){
		sendMessage(context, type, toNonNull(context.getString(resource, args)));
	}

	/**
	 * Send a message to a certain Address.
	 *
	 * @param context Context instance
	 * @param address destination Address
	 * @param resource String resource for the message text
	 */
	public static void sendMessage(final Context context, final Address address, final int resource){
		sendMessage(context, address, toNonNull(context.getString(resource)));
	}

	/**
	 * Send a message to a certain Address.
	 *
	 * @param context Context instance
	 * @param address destination Address
	 * @param resource String resource for the message text
	 * @param args format parameters for the resource
	 */
	public static void sendMessage(final Context context, final Address address, final int resource, final Object... args){
		sendMessage(context, address, toNonNull(context.getString(resource, args)));
	}

	/**
	 * Send a confirmation message to a certain Address. A confirm message is a message that:
	 * 1) Confers no information (except for confirming that the command was executed).
	 * 2) May be dropped if the user asked so.
	 *
	 * @param context Context instance
	 * @param address destination Address
	 * @param resource String resource for the message text
	 * @param args format parameters for the resource
	 */
	public static void sendConfirmMessage(final Context context, final Address address, final int resource, final Object... args){
		final SharedPreferences sp=PreferenceManager.getDefaultSharedPreferences(context);
		if(address.protocol != Protocol.SMS || !sp.getBoolean("expensive_sms", false))
			sendMessage(context, address, resource, args);
	}

	/**
	 * Send a notification to the user.
	 * 
	 * @param context Context instance
	 * @param type notification type
	 * @param msg the notification text
	 */
	public static void sendMessage(final Context context, final MessageType type,final String msg){
		final SharedPreferences sp=PreferenceManager.getDefaultSharedPreferences(context);
		final String address=sp.getString(type.toString(),null);
		if(address==null)
			return;
		sendMessage(context, new Address(address), msg);
	}

	/**
	 * Send a message to a certain Address.
	 *
	 * @param context Context instance
	 * @param address destination Address
	 * @param message the message text
	 */
	public static void sendMessage(final Context context, final Address address, final String message){
		switch(address.protocol){
		case HTTP:
			new HttpCallExecutableRunnable("/send", toNonNull(Arrays.asList(new Header("X-Destination", toNonNull(address.data)))), context, null, true, address.requestId == null ? message : address.requestId + " " + message).execute();
			break;

		case SMS:
			new Handler(Looper.getMainLooper()).post(new Runnable(){
					@Override
					public void run(){
						final ArrayList<String> parts = SmsManager.getDefault().divideMessage(message);
						SmsManager.getDefault().sendMultipartTextMessage(address.data, null, parts, null, null);
					}
				});
			break;

		case LOCAL:
			final Intent intent = new Intent(FonBotLocalActivity.RESPONSE_RECEIVED_ACTION);		
			intent.putExtra(FonBotLocalActivity.EXTRA_RESPONSE, message);
			context.sendBroadcast(intent);
			break;

		case NULL:
			break;//blackhole
		}
	}

	/**
	 * Splits a string into words.
	 *
	 * @param string the string
	 * @return an array of words
	 */
	public static String[] shellwords(final String string){
		return toNonNull(string.split("[ ]+(?=([^\"]*\"[^\"]*\")*[^\"]*$)"));//TODO: Make this handle backslash escapes
	}

	/**
	 * Returns the name associated with a phone number.
	 *
	 * @param context Context instance
	 * @param number phone number to search for
	 * @return the name associated with this number, or null if the number was not found in the contacts.
	 */
	public static @Nullable String callerId(final Context context, final String number){
		final Cursor cursor=context.getContentResolver().query(
				Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number)),
				new String[]{PhoneLookup.DISPLAY_NAME},
				null,
				null,
				null);

		if(cursor.moveToFirst()){
			final String name=cursor.getString(0);
			cursor.close();
			return name;
		}
		cursor.close();
		return null;
	}

	/**
	 * Registers an ongoing event.
	 *
	 * @param context Context instance
	 * @param event event to register
	 */
	public static void registerOngoing(final Context context, final OngoingEvent event){
		final Intent intent=new Intent(context, FonBotMainService.class);
		intent.setAction(FonBotMainService.ACTION_PUT_ONGOING);
		intent.putExtra(FonBotMainService.EXTRA_ONGOING_ID, event.ordinal());
		context.startService(intent);
	}

	/**
	 * Unregisters an ongoing event
	 *
	 * @param context Context instance
	 * @param event event to unregister
	 */
	public static void unregisterOngoing(final Context context, final OngoingEvent event){
		final Intent intent=new Intent(context, FonBotMainService.class);
		intent.setAction(FonBotMainService.ACTION_DELETE_ONGOING);
		intent.putExtra(FonBotMainService.EXTRA_ONGOING_ID, event.ordinal());
		context.startService(intent);
	}

	/**
	 * Gets the server URL according to the user preferences.
	 *
	 * @param context Context instance
	 * @param path URL path
	 * @return the server URL
	 * @throws MalformedURLException if the user preferences create an invalid URL
	 */
	public static URL getServerURL(final Context context, final String path) throws MalformedURLException{
		final String protocol=PreferenceManager.getDefaultSharedPreferences(context).getString("protocol", "https");
		final String hostname=PreferenceManager.getDefaultSharedPreferences(context).getString("hostname", "fonbot.ieval.ro");
		final int port=Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("port", "443"));
		final URL url=new URL(protocol, hostname, port, path);
		return url;
	}

	/**
	 * Executes a given command
	 *
	 * @param context Context instance
	 * @param cmd Command to execute
	 * @param args arguments for the command
	 * @param replyTo Address to send replies to
	 */
	private static void processCommand(final Context context, final Command cmd,final String[] args,final Address replyTo){
		if(Heavy.isCommandDisabled(context, cmd)){
			sendMessage(context, replyTo, command_disabled, cmd.toString());
			return;
		}

		switch(cmd){
		case TOAST:
			if(args.length < 1 || args.length > 2){
				Heavy.help(context, replyTo, toNonNull(Command.TOAST));
				break;
			}

			if(args.length==1)
				Heavy.toast(context, replyTo, toNonNull(args[0]));
			else {
				try {
					switch(ToastLength.valueOf(args[1].toUpperCase(Locale.ENGLISH))){
					case LONG:
						Heavy.toast(context, replyTo, toNonNull(args[0]), Toast.LENGTH_LONG);
						break;
					case SHORT:
						Heavy.toast(context, replyTo, toNonNull(args[0]), Toast.LENGTH_SHORT);
					}
				} catch(IllegalArgumentException e){
					sendMessage(context, replyTo, invalid_length_allowed_values_are, join(", ",toNonNull(ToastLength.values())));
				}
			}
			break;

		case ECHO:
			if(args.length==0){
				Heavy.help(context, replyTo, toNonNull(Command.ECHO));
				break;
			}
			sendMessage(context, replyTo, join(" ",args));
			break;

		case SMS:
			if(args.length < 2){
				Heavy.help(context, replyTo, toNonNull(Command.SMS));
				break;
			}
			Heavy.sms(context, replyTo, toNonNull(args[0]), join(" ", 1, args), false);
			break;

		case SMSQ:
			if(args.length < 2){
				Heavy.help(context, replyTo, toNonNull(Command.SMSQ));
				break;
			}
			Heavy.sms(context, replyTo, toNonNull(args[0]), join(" ", 1, args), true);
			break;

		case FLASH:
			if(args.length != 1){
				Heavy.help(context, replyTo, toNonNull(Command.FLASH));
				break;
			}

			try {
				Heavy.flash(context, replyTo, OnOff.valueOf(args[0].toUpperCase(Locale.ENGLISH)) == OnOff.ON);
			} catch(IllegalArgumentException e) {
				sendMessage(context, replyTo, could_not_parse_argument_allowed_values_are, join(", ", toNonNull(OnOff.values())));
			}
			break;

		case WIFI:
			if(args.length>1){
				Heavy.help(context, replyTo, toNonNull(Command.WIFI));
				break;
			}
			if(args.length==0)
				Heavy.wifi(context, replyTo);
			else {
				try {
					Heavy.wifi(context, replyTo, OnOff.valueOf(args[0].toUpperCase(Locale.ENGLISH)) == OnOff.ON);
				} catch(IllegalArgumentException e) {
					sendMessage(context, replyTo, could_not_parse_argument_allowed_values_are, join(", ", toNonNull(OnOff.values())));
				}
			}
			break;

		case BLUETOOTH:
			if(args.length>1){
				Heavy.help(context, replyTo, toNonNull(Command.BLUETOOTH));
				break;
			}
			if(args.length==0)
				Heavy.bluetooth(context, replyTo);
			else {
				try {
					Heavy.bluetooth(context, replyTo, OnOff.valueOf(args[0].toUpperCase(Locale.ENGLISH)) == OnOff.ON);
				} catch(IllegalArgumentException e) {
					sendMessage(context, replyTo, could_not_parse_argument_allowed_values_are, join(", ", toNonNull(OnOff.values())));
				}
			}
			break;

		case DIAL:
			if(args.length!=1){
				Heavy.help(context, replyTo, toNonNull(Command.DIAL));
				break;
			}
			Heavy.dial(context, replyTo, toNonNull(args[0]));
			break;

		case RING:
			if(args.length>1){
				Heavy.help(context, replyTo, toNonNull(Command.RING));
				break;
			}
			if(args.length==0)
				Heavy.ring(context, replyTo);
			else {
				try {
					Heavy.ring(context, replyTo, OnOff.valueOf(args[0].toUpperCase(Locale.ENGLISH)) == OnOff.ON);
				} catch(IllegalArgumentException e){
					sendMessage(context, replyTo, could_not_parse_argument_allowed_values_are, join(", ", toNonNull(OnOff.values())));
				}
			}
			break;

		case SPEAK:
			if(args.length==0){
				Heavy.help(context, replyTo, toNonNull(Command.SPEAK));
				break;
			}
			Heavy.speak(context, replyTo, join(" ",args));
			break;

		case VIBRATE:
			if(args.length!=1){
				Heavy.help(context, replyTo, toNonNull(Command.VIBRATE));
				break;
			}
			final long ms;
			try{
				ms=Long.parseLong(args[0]);
			} catch(NumberFormatException e){
				sendMessage(context, replyTo, could_not_parse_ms);
				break;
			}
			Heavy.vibrate(context, replyTo, ms);
			break;

		case DIALOG://TODO: Should add an edittext
			if(args.length<1){
				Heavy.help(context, replyTo, toNonNull(Command.DIALOG));
				break;
			}
			final String[] buttons=new String[args.length-1];
			System.arraycopy(args,1,buttons,0,buttons.length);
			Heavy.dialog(context, replyTo, toNonNull(args[0]), buttons);
			break;

		case LOCATION:
			if(args.length>3||args.length<1){
				Heavy.help(context, replyTo, toNonNull(Command.LOCATION));
				break;
			}

			final @NonNull String provider;
			try{
				switch(LocationProvider.valueOf(args[0].toUpperCase(Locale.ENGLISH))){
				case GPS:
					provider=toNonNull(LocationManager.GPS_PROVIDER);
					break;
				case NETWORK:
				default:
					provider=toNonNull(LocationManager.NETWORK_PROVIDER);
					break;
				}
			} catch(IllegalArgumentException e){
				sendMessage(context, replyTo, cannot_parse_provider_allowed_values_are, join(", ",toNonNull(LocationProvider.values())));
				break;
			}

			final long minTime;
			final float minDistance;

			if(args.length>1)
				try{
					minTime=Long.parseLong(args[1]);
				} catch (NumberFormatException e){
					sendMessage(context, replyTo, cannot_parse_min_time);
					break;
				}
			else
				minTime=500;

			if(args.length>2)
				try{
					minDistance=Float.parseFloat(args[2]);
				} catch (NumberFormatException e){
					sendMessage(context, replyTo, cannot_parse_min_distance);
					break;
				}
			else
				minDistance=0;
			Heavy.location(context, replyTo, provider, minTime, minDistance);
			break;

		case NOLOCATION:
			Heavy.nolocation(context, replyTo);
			break;

		case RINGER:
			if(args.length>1){
				Heavy.help(context, replyTo, toNonNull(Command.RINGER));
				break;
			}
			if(args.length==0)
				Heavy.ringer(context, replyTo);
			else{
				try{
					final RingerMode rm=RingerMode.valueOf(args[0].toUpperCase(Locale.ENGLISH));
					switch(rm){
					case NORMAL:
						Heavy.ringer(context, replyTo, AudioManager.RINGER_MODE_NORMAL);
						break;
					case VIBRATE:
						Heavy.ringer(context, replyTo, AudioManager.RINGER_MODE_VIBRATE);
						break;
					case SILENT:
						Heavy.ringer(context, replyTo, AudioManager.RINGER_MODE_SILENT);
						break;
					}
				} catch (IllegalArgumentException e){
					Utils.sendMessage(context, replyTo, invalid_ringer_mode_valid_values_are, join(", ",toNonNull(RingerMode.values())));
				}
			}
			break;

		case NCFILE:
			if(args.length!=3){
				Heavy.help(context, replyTo, toNonNull(Command.NCFILE));
				break;
			}

			final int ncfilePort;
			try{
				ncfilePort=Integer.parseInt(args[2]);
			} catch (NumberFormatException e){
				sendMessage(context, replyTo, cannot_parse_port);
				break;
			}
			Heavy.ncfile(context, replyTo, toNonNull(args[0]), toNonNull(args[1]), ncfilePort);
			break;

		case PHOTO:
			if(args.length!=3){
				Heavy.help(context, replyTo, toNonNull(Command.PHOTO));
				break;
			}

			final int cameraNumber;
			try{
				cameraNumber=Integer.parseInt(args[0]);
			} catch (NumberFormatException e){
				sendMessage(context, replyTo, cannot_parse_camera_number);
				break;
			}

			final int photoPort;
			try{
				photoPort=Integer.parseInt(args[2]);
			} catch (NumberFormatException e){
				sendMessage(context, replyTo, cannot_parse_port);
				break;
			}
			Heavy.photo(context, replyTo, cameraNumber, toNonNull(args[1]), photoPort);
			break;

		case SETNOTIFICATION:
			if(args.length!=1){
				Heavy.help(context, replyTo, toNonNull(Command.SETNOTIFICATION));
				break;
			}

			try{
				PreferenceManager.getDefaultSharedPreferences(context).edit()
				.putString(MessageType.valueOf(args[0].toUpperCase(Locale.ENGLISH)).toString(), replyTo.toString())
				.commit();
				sendConfirmMessage(context, replyTo, notification_enabled);
			} catch (IllegalArgumentException e){
				sendMessage(context, replyTo, messagetype_should_be_one_of, join(", ",toNonNull(MessageType.values())));
				break;
			}

			break;

		case DELNOTIFICATION:
			if(args.length!=1){
				Heavy.help(context, replyTo, toNonNull(Command.DELNOTIFICATION));
				break;
			}

			try{
				PreferenceManager.getDefaultSharedPreferences(context).edit()
				.remove(MessageType.valueOf(args[0].toUpperCase(Locale.ENGLISH)).toString())
				.commit();
				sendConfirmMessage(context, replyTo,  notification_disabled);
			} catch (IllegalArgumentException e){
				sendMessage(context, replyTo,  messagetype_should_be_one_of, join(", ",toNonNull(MessageType.values())));
				break;
			}

			
			break;
		case SETPASSWORD:
			if(args.length > 1){
				Heavy.help(context, replyTo, toNonNull(Command.SETPASSWORD));
				break;
			}

			try{
				if(args.length==0)
					Heavy.setPassword(context, replyTo);
				else
					Heavy.setPassword(context, replyTo, toNonNull(args[0]));
			} catch (SecurityException e){
				sendMessage(context, replyTo,  security_exception, e.getMessage());
			}
			break;

		case WIPE:
			if(args.length!=2){
				Heavy.help(context, replyTo, toNonNull(Command.WIPE));
				break;
			}

			if(!args[1].equalsIgnoreCase(WIPE_CONFIRM_STRING)){
				sendMessage(context, replyTo, the_second_argument_to_wipe_must_be, WIPE_CONFIRM_STRING);
				break;
			}

			try{
				Heavy.wipe(context, toNonNull(WipeType.valueOf(args[0].toUpperCase(Locale.ENGLISH))));
			} catch (IllegalArgumentException e){
				sendMessage(context, replyTo, wipetype_should_be_one_of, join (", ",toNonNull(WipeType.values())));
			} catch (SecurityException e){
				sendMessage(context, replyTo, security_exception, e.getMessage());
			}
			break;

		case LOCK:
			try{
				Heavy.lock(context, replyTo);
			} catch (SecurityException e){
				sendMessage(context, replyTo, security_exception, e.getMessage());
			}
			break;

		case VIEW:
			if(args.length!=1){
				Heavy.help(context, replyTo, toNonNull(Command.VIEW));
				break;
			}

			Heavy.view(context, replyTo, toNonNull(Uri.parse(args[0])));
			break;

		case PLAY:
			Heavy.musicPlayerCommand(context, replyTo, "play");
			break;

		case PAUSE:
			Heavy.musicPlayerCommand(context, replyTo, "pause");
			break;

		case NEXT:
			Heavy.musicPlayerCommand(context, replyTo, "next");
			break;

		case PREV:
			Heavy.musicPlayerCommand(context, replyTo, "previous");
			break;

		case BATT:
			Heavy.batt(context, replyTo);
			break;

		case CALLLOG:
			if (args.length > 1) {
				Heavy.help(context, replyTo, toNonNull(Command.CALLLOG));
				break;
			}

			if (args.length == 0)
				Heavy.calllog(context, replyTo, 5);
			else {
				try {
					Heavy.calllog(context, replyTo, Integer.parseInt(args[0]));
				} catch (IllegalArgumentException e){
					sendMessage(context, replyTo, cannot_parse_count);
				}
			}
			break;

		case SMSLOG:
			if (args.length > 1) {
				Heavy.help(context, replyTo, toNonNull(Command.SMSLOG));
				break;
			}

			if (args.length == 0)
				Heavy.smslog(context, replyTo, 5);
			else {
				try {
					Heavy.smslog(context, replyTo, Integer.parseInt(args[0]));
				} catch (IllegalArgumentException e) {
					sendMessage(context, replyTo, cannot_parse_count);
				}
			}
			break;

		case HELP:
			if(args.length != 1){
				Heavy.help(context, replyTo, toNonNull(Command.HELP));
				break;
			}

			try {
				Heavy.help(context, replyTo, toNonNull(Command.valueOf(args[0].toUpperCase(Locale.ENGLISH))));
			} catch (IllegalArgumentException e) {
				sendMessage(context, replyTo, no_such_command_command_list, join(", ", toNonNull(Command.values())));
			}
			break;

		case LS:
			if(args.length != 1){
				Heavy.help(context, replyTo, toNonNull(Command.LS));
				break;
			}

			Heavy.ls(context, replyTo, toNonNull(args[0]));
			break;

		case RM:
			if(args.length != 1){
				Heavy.help(context, replyTo, toNonNull(Command.RM));
				break;
			}

			Heavy.rm(context, replyTo, toNonNull(args[0]));
			break;

		case CONTACTS:
			if(args.length != 1){
				Heavy.help(context, replyTo, toNonNull(Command.CONTACTS));
				break;
			}

			Heavy.contacts(context, replyTo, toNonNull(args[0]));
			break;

		case DISABLE:
			if(replyTo.protocol != Protocol.LOCAL || args.length != 1){
				Heavy.help(context, replyTo, toNonNull(Command.DISABLE));
				break;
			}

			try{
				Heavy.disable(context, replyTo, toNonNull(Command.valueOf(args[0].toUpperCase(Locale.ENGLISH))));
			} catch (IllegalArgumentException e){
				sendMessage(context, replyTo, no_such_command_command_list, join(", ", toNonNull(Command.values())));
			}
			break;

		case ENABLE:
			if(replyTo.protocol != Protocol.LOCAL || args.length != 1){
				Heavy.help(context, replyTo, toNonNull(Command.ENABLE));
				break;
			}

			try{
				Heavy.enable(context, replyTo, toNonNull(Command.valueOf(args[0].toUpperCase(Locale.ENGLISH))));
			} catch (IllegalArgumentException e){
				sendMessage(context, replyTo, no_such_command_command_list, join(", ", toNonNull(Command.values())));
			}
			break;

		case POLL:
			Heavy.poll(context, replyTo);
			break;

		case HANGUP:
			Heavy.hangup(context, replyTo);
			break;

		case ANSWER:
			Heavy.answer(context, replyTo);
			break;

		case LAUNCH:
			if(args.length!=1){
				Heavy.help(context, replyTo, toNonNull(Command.LAUNCH));
				break;
			}
			Heavy.launch(context, replyTo, toNonNull(args[0]));
			break;

		case DATA:
			if(args.length>1){
				Heavy.help(context, replyTo, toNonNull(Command.DATA));
				break;
			}

			if(args.length==0){
				Heavy.data(context, replyTo);
				break;
			}
			try {
				Heavy.data(context, replyTo, OnOff.valueOf(args[0].toUpperCase(Locale.ENGLISH)) == OnOff.ON);
			} catch(IllegalArgumentException e) {
				sendMessage(context, replyTo, could_not_parse_argument_allowed_values_are, join(", ", toNonNull(OnOff.values())));
			}
			break;

		case GPS:
			if(args.length>1){
				Heavy.help(context, replyTo, toNonNull(Command.GPS));
				break;
			}

			if(args.length==0){
				Heavy.gps(context, replyTo);
				break;
			}

			try {
				Heavy.gps(context, replyTo, OnOff.valueOf(args[0].toUpperCase(Locale.ENGLISH)) == OnOff.ON);
			} catch(IllegalArgumentException e) {
				sendMessage(context, replyTo, could_not_parse_argument_allowed_values_are, join(", ", toNonNull(OnOff.values())));
			}
			break;

		case GLOCATION:
			if(args.length>1){
				Heavy.help(context, replyTo, toNonNull(Command.GLOCATION));
				break;
			}

			if(args.length==0){
				Heavy.glocation(context, replyTo);
				break;
			}

			try {
				Heavy.glocation(context, replyTo, OnOff.valueOf(args[0].toUpperCase(Locale.ENGLISH)) == OnOff.ON);
			} catch(IllegalArgumentException e) {
				sendMessage(context, replyTo, could_not_parse_argument_allowed_values_are, join(", ", toNonNull(OnOff.values())));
			}
			break;

		case REBOOT:
			if(args.length>1){
				Heavy.help(context, replyTo, toNonNull(Command.REBOOT));
				break;
			}

			Heavy.reboot(context, replyTo, args.length==0?null:args[0]);
			break;

		case NOTIFY:
			if(args.length!=1 && args.length!=3){
				Heavy.help(context, replyTo, toNonNull(Command.NOTIFY));
				return;
			}

			final int id;
			try{
				id=Integer.parseInt(args[0]);
			} catch (NumberFormatException e){
				sendMessage(context, replyTo, R.string.could_not_parse_id);
				break;
			}

			if(args.length==1)
				Heavy.notify(context, replyTo, id);
			else
				Heavy.notify(context, replyTo, id, toNonNull(args[1]), toNonNull(args[2]));
			break;

		case SCREENCAP:
			if(args.length != 1){
				Heavy.help(context, replyTo, toNonNull(Command.SCREENCAP));
				return;
			}

			Heavy.screencap(context, replyTo, args[0]);
			break;

		case TORCH:
			Heavy.torch(context, replyTo);
			break;

		case GETFILE:
			if(args.length != 3){
				Heavy.help(context, replyTo, toNonNull(Command.GETFILE));
				return;
			}

			final int getfilePort;
			try{
				getfilePort=Integer.parseInt(args[2]);
			} catch (NumberFormatException e){
				sendMessage(context, replyTo, cannot_parse_port);
				break;
			}
			Heavy.getfile(context, replyTo, toNonNull(args[0]), toNonNull(args[1]), getfilePort);
			break;

		case SH:
			if(args.length == 0){
				Heavy.help(context, replyTo, toNonNull(Command.SH));
				return;
			}

			Heavy.execute(context, replyTo, "sh", join(" ", args));
			break;

		case ROOTSH:
			if(args.length == 0){
				Heavy.help(context, replyTo, toNonNull(Command.ROOTSH));
				return;
			}

			Heavy.execute(context, replyTo, "su", join(" ", args));
			break;
		}

	}

	/**
	 * Executes a given command
	 *
	 * @param context Context instance
	 * @param cmd Command to execute
	 * @param args arguments for the command
	 * @param replyTo Address to send replies to
	 */
	public static void processCommand(final Context context, final String cmd,final String[] args,final Address replyTo){
		final @NonNull Command command;
		try{
			command=toNonNull(Command.valueOf(cmd.toUpperCase(Locale.ENGLISH)));
		} catch (IllegalArgumentException e){
			sendMessage(context, replyTo, unknown_command, cmd.toUpperCase(Locale.ENGLISH), e.getMessage());
			return;
		}

		try{
			processCommand(context, command,args,replyTo);
		} catch(Exception e){
			sendMessage(context, replyTo, error_while_processing_command, e.getClass().getName(), e.getMessage());
			Log.w(Utils.class.getName(), "Error while processing command", e);
		}
	}
}
