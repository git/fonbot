package ro.ieval.fonbot;

import static ro.ieval.fonbot.Utils.toNonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Collections;

import org.eclipse.jdt.annotation.Nullable;

import android.content.Context;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * UncaughtExceptionHandler that sends exceptions to the server.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
final class RemoteCrashdumpHandler implements UncaughtExceptionHandler {
	/** Context instance */
	private final Context context;

	/**
	 * Constructs a RemoteCrashdumpHandler with a given Context instance.
	 * 
	 * @param context Context instance
	 */
	public RemoteCrashdumpHandler(final Context context) {
		this.context=context;
	}

	@Override
	public void uncaughtException(final @Nullable Thread thread, final @Nullable Throwable ex) {
		if(ex==null)
			return;

		final ByteArrayOutputStream baos=new ByteArrayOutputStream(16384);
		final PrintStream pw=new PrintStream(baos);
		ex.printStackTrace(pw);
		pw.close();

		new HttpCallExecutableRunnable("/crashdump", toNonNull(Collections.<Header>emptyList()),
				toNonNull(context), null, false, toNonNull(baos.toByteArray())).execute();

		try {
			baos.close();
		} catch (IOException e) {//NOPMD close exceptions are unimportant
			//ignored
		}
	}

}
