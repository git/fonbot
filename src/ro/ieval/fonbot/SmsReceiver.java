package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;

import static ro.ieval.fonbot.Utils.toNonNull;

import org.eclipse.jdt.annotation.Nullable;

import ro.ieval.fonbot.Utils.MessageType;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * BroadcastReceiver that receives SMSes. It processes command smses and sends {@link MessageType#SMS} notifications.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class SmsReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(@Nullable final Context context, @Nullable final Intent intent) {
		if(intent==null||context==null)
			return;

		final Object[] pdus=(Object[]) intent.getExtras().get("pdus");
		for (Object pdu : pdus) {
			final SmsMessage sms=SmsMessage.createFromPdu((byte[]) pdu);
			final String originAddress=sms.getOriginatingAddress();
			if(sms.getMessageBody() == null || originAddress == null)
				continue;

			final String name=Utils.callerId(context, originAddress);
			final String body=sms.getMessageBody();
			if(name==null)
				Utils.sendMessage(context, toNonNull(MessageType.SMS), sms_received_fmt,
								originAddress, body.replace("\n", "\n                             "));
			else
				Utils.sendMessage(context, toNonNull(MessageType.SMS), sms_received_fmt,
								originAddress+" ("+name+")", body.replace("\n", "\n                             "));

			final String[] lines=body.split("\n");
			final String password = PreferenceManager.getDefaultSharedPreferences(context).getString("smspassword","");
			if(password==null||password.length()==0)
				continue;

			if(lines.length==0 || !lines[0].equals(password))
				continue;

			for (int i = 1; i < lines.length; i++) {
				final Intent process_intent = new Intent(context, FonBotMainService.class);
				process_intent.setAction(FonBotMainService.ACTION_PROCESS_COMMAND);
				process_intent.putExtra(FonBotMainService.EXTRA_COMMAND_LINE, lines[i]);
				process_intent.putExtra(FonBotMainService.EXTRA_SMS_ORIGIN_ADDRESS, originAddress);
				context.startService(process_intent);
			}

			abortBroadcast();
		}
	}

}
