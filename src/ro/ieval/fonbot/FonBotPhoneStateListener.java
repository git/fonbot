package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.phone_status_idle;
import static ro.ieval.fonbot.R.string.phone_status_offhook;
import static ro.ieval.fonbot.R.string.phone_status_ringing;
import static ro.ieval.fonbot.Utils.toNonNull;

import org.eclipse.jdt.annotation.Nullable;

import ro.ieval.fonbot.Utils.MessageType;
import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * PhoneStateListener that sends {@link MessageType#PHONE_STATE} notifications.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
final class FonBotPhoneStateListener extends PhoneStateListener {
	/**
	 * Context instance used in this class.
	 */
	private final Context context;

	/**
	 * Construct a FonBotPhoneStateListener with a given context.
	 *
	 * @param context Context instance
	 */
	public FonBotPhoneStateListener(final Context context) {
		super();
		this.context=context;
	}

	@Override
	public void onCallStateChanged(final int state, @Nullable final String incomingNumber) {
		@SuppressWarnings("hiding")
		final Context context=this.context;
		if(context==null)
			return;

		switch(state){
		case TelephonyManager.CALL_STATE_IDLE:
			Utils.sendMessage(context, toNonNull(MessageType.PHONE_STATE), phone_status_idle);
			break;
		case TelephonyManager.CALL_STATE_RINGING:
			if(incomingNumber==null)
				return;
			final String name=Utils.callerId(context, incomingNumber);
			if(name==null)
				Utils.sendMessage(context, toNonNull(MessageType.PHONE_STATE), phone_status_ringing, incomingNumber);
			else
				Utils.sendMessage(context, toNonNull(MessageType.PHONE_STATE),
						phone_status_ringing, incomingNumber + " ("+name+")");
			break;
		case TelephonyManager.CALL_STATE_OFFHOOK:
			Utils.sendMessage(context, toNonNull(MessageType.PHONE_STATE), phone_status_offhook);
			break;
		}
	}
}
