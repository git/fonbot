package ro.ieval.fonbot;

import java.io.IOException;

import org.eclipse.jdt.annotation.Nullable;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * Preference activity. Lets the user change app preferences.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class FonBotPreferenceActivity extends PreferenceActivity {
	/**
	 * Preference for toggling device admin permissions.
	 */
	private CheckBoxPreference adminPreference;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(@Nullable final Bundle icicle) {
		super.onCreate(icicle);
		setTitle("FonBot Preferences");
		addPreferencesFromResource(R.xml.prefs);

		adminPreference=(CheckBoxPreference) findPreference("admin");
		final DevicePolicyManager dpm=(DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);
		final ComponentName adminReceiver=new ComponentName(this,FonBotAdminReceiver.class);
		adminPreference.setChecked(dpm.isAdminActive(adminReceiver));

		adminPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(final @Nullable Preference preference, final @Nullable Object newValue) {
				if(newValue==null){
					Log.wtf(FonBotPreferenceActivity.class.getName(), "newValue in OnPreferenceChange is null");
					throw new AssertionError("Log.wtf did not terminate the process");
				}

				final boolean isChecked=((Boolean) newValue).booleanValue();
				if(isChecked){
					if(dpm.isAdminActive(adminReceiver))
						return true;

					final Intent intent=new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
					intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminReceiver);
					startActivityForResult(intent, 0);
					return false;
				}
				dpm.removeActiveAdmin(adminReceiver);
				return true;
			}
		});

		final CheckBoxPreference foregroundPreference=(CheckBoxPreference) findPreference("foreground");
		foregroundPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(final @Nullable Preference preference, final @Nullable Object newValue) {
				startService(new Intent(FonBotPreferenceActivity.this, FonBotMainService.class));
				return true;
			}
		});

		final CheckBoxPreference systemPreference=(CheckBoxPreference) findPreference("system");
		final ApplicationInfo info=getApplicationInfo();
		final boolean isSystem=(info.flags&ApplicationInfo.FLAG_SYSTEM)!=0;
		systemPreference.setChecked(isSystem);
		systemPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(final @Nullable Preference preference, final @Nullable Object newValue) {
				if(newValue==null)
					return false;
				final boolean isChecked=((Boolean)newValue).booleanValue();
				if(isChecked==isSystem)
					return true;

				try {
					final String privapp = android.os.Build.VERSION.SDK_INT < 19 ? "app" : "priv-app";
					final String remountCommand="mount -o remount,rw /system";
					final String copyToSystemCommand="cp "+info.sourceDir+" /system/" + privapp + "/FonBot.apk";
					final String chmodSystemCommand="chmod 644 /system/" + privapp + "/FonBot.apk";
					final String rmSystemCommand="rm /system/" + privapp + "/FonBot.apk";
					final String copyToUserCommand="cp "+info.sourceDir+" /data/app/FonBot.apk";
					final String chmodUserCommand="chmod 644 /data/app/FonBot.apk";
					final String rmUserCommand="rm "+info.sourceDir;

					if(isChecked){
						Runtime.getRuntime().exec(new String[]{
								"su", "-c",
								remountCommand+';'+copyToSystemCommand+';'+chmodSystemCommand+';'+rmUserCommand
						}).waitFor();

						Toast.makeText(FonBotPreferenceActivity.this,
								"Reboot to make FonBot a system application", Toast.LENGTH_LONG).show();
					} else {
						Runtime.getRuntime().exec(new String[]{
								"su", "-c",
								remountCommand+';'+copyToUserCommand+';'+chmodUserCommand+';'+rmSystemCommand
						}).waitFor();
						Toast.makeText(FonBotPreferenceActivity.this,
								"Reboot to make FonBot a non-system application", Toast.LENGTH_LONG).show();
					}
				} catch (IOException e) {
					return false;
				} catch (InterruptedException e) {
					return false;
				}
				return true;
			}
		});

		final EditTextPreference protocolPreference=(EditTextPreference) findPreference("protocol");
		protocolPreference.setSummary(protocolPreference.getText());
		protocolPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(final @Nullable Preference preference, final @Nullable Object newValue) {
				if(newValue==null)
					return false;

				protocolPreference.setSummary(newValue.toString());
				return true;
			}
		});

		final EditTextPreference hostnamePreference=(EditTextPreference) findPreference("hostname");
		hostnamePreference.setSummary(hostnamePreference.getText());
		hostnamePreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(final @Nullable Preference preference, final @Nullable Object newValue) {
				if(newValue==null)
					return false;

				hostnamePreference.setSummary(newValue.toString());
				return true;
			}
		});

		final EditTextPreference portPreference=(EditTextPreference) findPreference("port");
		portPreference.setSummary(portPreference.getText());
		portPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(final @Nullable Preference preference, final @Nullable Object newValue) {
				if(newValue==null)
					return false;

				try{
					Integer.parseInt(newValue.toString());
				} catch (NumberFormatException e){
					return false;
				}
				portPreference.setSummary(newValue.toString());
				return true;
			}
		});
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
		final DevicePolicyManager dpm=(DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);
		final ComponentName adminReceiver=new ComponentName(this,FonBotAdminReceiver.class);
		adminPreference.setChecked(dpm.isAdminActive(adminReceiver));
	}

}
