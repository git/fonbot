package ro.ieval.fonbot;

import static ro.ieval.fonbot.Utils.toNonNull;

import java.io.InputStream;
import org.eclipse.jdt.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import ro.ieval.fonbot.Address.Protocol;
import ro.ieval.fonbot.HttpCallExecutableRunnable.ResultCallback;

/*
* Copyright © 2013 Marius Gavrilescu
* 
* This file is part of FonBot.
*
* FonBot is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FonBot is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
*/

/**
 * ResultCallback implementation that polls the server for pending commands.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
final class PollResultCallback implements ResultCallback {
	/** Context instance */
	private final Context context;

	/**
	 * Construct a <code>PollResultCallback</code> with a Context.
	 *
	 * @param context Context instance
	 */
	public PollResultCallback(final Context context) {
		this.context=context;
	}

	@Override
	public void onResult(final int responseCode, final String responseMessage, final @Nullable InputStream inputStream) {
		if(responseCode != 200 || inputStream==null){
			if(responseCode != 204 && responseCode != 504)
				throw new RuntimeException("Bad HTTP response code: "+responseCode);
			return;
		}

		final Handler handler=new Handler(Looper.getMainLooper());

		try{
			final JSONArray array;
			{
				final byte[] buf=new byte[2048*1024];
				final int length=inputStream.read(buf);
				array=new JSONArray(new String(buf, 0, length));
			}

			for(int i=0;i<array.length();i++){
				final JSONObject object=array.getJSONObject(i);

				final JSONArray jsonargs=object.getJSONArray("args");
				final String command=object.getString("command");
				final Address replyTo=new Address(toNonNull(Protocol.HTTP), object.getString("replyto"), object.has("requestid") ? object.getString("requestid") : null);//NOPMD address depends on command
				final String[] args=new String[jsonargs.length()];
				for(int j=0;j<args.length;j++)
					args[j]=jsonargs.getString(j);

				Log.d(getClass().getName(), "Poll got command "+command+" with "+((args.length==0)?"no args":"args "+Utils.join(
						" ",toNonNull(args))));

				handler.post(new Runnable() {
					@Override
					public void run() {
						Utils.processCommand(
								toNonNull(context),
								toNonNull(command),
								args, replyTo);
					}
				});
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onError(final String error) {
		//error handling is done by the LongPollRunnable thread
	}

}
