package ro.ieval.fonbot;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * POJO that represents a HTTP header
 * 
 * @author Marius Gavrilescu
 */
final class Header{
	/**
	 * The header name
	 */
	public final String name;
	/**
	 * The header value
	 */
	public final String value;

	/**
	 * Constructs a Header from its two parts.
	 *
	 * @param name the name of the header
	 * @param value the value of the header
	 */
	public Header(final String name, final String value){
		this.name=name;
		this.value=value;
	}
}
