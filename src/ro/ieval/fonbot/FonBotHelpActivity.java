package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;

import org.eclipse.jdt.annotation.Nullable;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * An activity that shows FonBot help.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class FonBotHelpActivity extends Activity {
	@Override
	protected void onCreate(final @Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final TextView textView=new TextView(this);
		textView.setText(help);
		textView.setMovementMethod(LinkMovementMethod.getInstance());
		setTitle(help_activity_label);
		setContentView(textView);
	}
}
