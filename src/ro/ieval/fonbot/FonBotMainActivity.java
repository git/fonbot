package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.logging_in;
import static ro.ieval.fonbot.Utils.toNonNull;

import java.io.InputStream;
import org.eclipse.jdt.annotation.Nullable;

import ro.ieval.fonbot.FonBotMainService.Binder;
import ro.ieval.fonbot.HttpCallExecutableRunnable.ResultCallback;
import ro.ieval.fonbot.Utils.OngoingEvent;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * Main Activity. Shows the login status and a list of ongoing events.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class FonBotMainActivity extends ListActivity {
	/**
	 * Adapter for the ongoing event list. Shows a list of ongoing events with cancel buttons.
	 *
	 * @author Marius Gavrilescu 
	 */
	private static final class ArrayAdapter extends android.widget.ArrayAdapter<OngoingEvent>{
		/**
		 * Constructs an ArrayAdapter with a given list of events
		 *
		 * @param context Context instance
		 * @param objects the list of events
		 */
		public ArrayAdapter(final Context context, final OngoingEvent[] objects) {
			super(context, 0, 0, objects);
		}

		@Override
		public View getView(final int position, final @Nullable View convertView, final @Nullable ViewGroup parent) {
			final LayoutInflater inflater=(LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
			final View listItem;

			if(convertView instanceof LinearLayout){
				listItem=convertView;
			} else
				listItem=inflater.inflate(R.layout.ongoing_list_item, parent, false);

			final Button button=(Button) listItem.findViewById(R.id.ongoingItemButton);
			final TextView textView=(TextView) listItem.findViewById(R.id.ongoingItemTextView);
			final OngoingEvent event=getItem(position);

			textView.setText(event.resource);
			button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final @Nullable View v) {
					Heavy.cancelOngoing(toNonNull(getContext()), event);
				}
			});

			return listItem;
		}
	}

	/**
	 * ServiceConnection for getting the ongoing event list from {@link FonBotMainService}
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	private final class ServiceConnection implements android.content.ServiceConnection{
		/** Binder got from onServiceConnected */
		private Binder binder;

		@Override
		public void onServiceDisconnected(final @Nullable ComponentName name) {
			// do nothing
		}

		@Override
		public void onServiceConnected(final @Nullable ComponentName name, final @Nullable IBinder service) {
			if(service==null)
				return;
			binder=(Binder) service;
			refreshAdapter();
		}

		/**
		 * Updates the list of ongoing events from the service.
		 */
		public void refreshAdapter(){
			if(binder==null)
				return;
			setListAdapter(new ArrayAdapter(FonBotMainActivity.this,
					toNonNull(binder.getService().getOngoingEvents().toArray(new OngoingEvent[0]))));
		}
	}

	/**
	 * BroadcastReceiver that refreshes the ongoing event list.
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	private final class OngoingUpdateReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(final @Nullable Context context, final @Nullable Intent intent) {
			connection.refreshAdapter();
		}
	}

	/**
	 * Implementation of <code>ResultCallback</code> that updates the {@link #resultTextView}
	 *
	 * @author Marius Gavrilescu
	 */
	public final class UpdateResultCallback implements ResultCallback{
		@Override
		public void onResult(final int responseCode, final String responseMessage, final InputStream inputStream) {
			if(responseCode>=200 && responseCode<300)
				updateResultTextView(responseMessage, false);
			else
				updateResultTextView(responseMessage, true);
		}

		@Override
		public void onError(final String error) {
			updateResultTextView(error, true);
		}

		/**
		 * Update the {@link #resultTextView}.
		 *
		 * @param message new text for the TextView
		 * @param error true if the text is an error message, false otherwise
		 */
		private void updateResultTextView(final String message, final boolean error){
			handler.post(new Runnable() {
				@Override
				public void run() {
					resultTextView.setText(message);
					if(error)
						resultTextView.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cross, 0, 0);
					else {
						resultTextView.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tick, 0, 0);
						startService(new Intent(FonBotMainActivity.this, FonBotMainService.class));
					}
				}
			});
		}
	}

	/**
	 * The one instance of {@link OngoingUpdateReceiver}
	 */
	private final BroadcastReceiver ongoingUpdateReceiver=new OngoingUpdateReceiver();
	/**
	 * IntentFilter for {@link #ongoingUpdateReceiver}
	 */
	private static final IntentFilter ONGOING_UPDATE_FILTER=new IntentFilter(FonBotMainService.ACTION_ONGOING_UPDATE);
	/**
	 * The one instance of {@link UpdateResultCallback}
	 */
	private final UpdateResultCallback updateResultCallback=new UpdateResultCallback();
	/**
	 * The one instance of {@link ServiceConnection}
	 */
	private final ServiceConnection connection=new ServiceConnection();

	/**
	 * TextView that tells the user whether logging in failed or succeded.
	 */
	private TextView resultTextView;
	/** Handler instance */
	private Handler handler;

	@Override
	protected void onCreate(@Nullable final Bundle icicle) {
		super.onCreate(icicle);

		setContentView(R.layout.main);
		handler=new Handler();
		resultTextView=(TextView) findViewById(R.id.resultTextView);
	}

	@Override
	protected void onStart() {
		super.onStart();
		resultTextView.setText(logging_in);
		resultTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		new HttpCallExecutableRunnable("/ok", null,
				toNonNull(getApplicationContext()), toNonNull(updateResultCallback), false).execute();
		connection.refreshAdapter();
	}

	@Override
	protected void onResume() {
		super.onResume();
		LocalBroadcastManager.getInstance(this).registerReceiver(ongoingUpdateReceiver, ONGOING_UPDATE_FILTER);
		bindService(new Intent(this, FonBotMainService.class), connection, 0);
	}

	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(ongoingUpdateReceiver);
		unbindService(connection);
	}

	@Override
	public boolean onCreateOptionsMenu(@Nullable final Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(@Nullable final MenuItem item) {
		if(item==null)
			return super.onOptionsItemSelected(item);
		switch(item.getItemId()){
		case R.id.prefsItem:
			startActivity(new Intent(this, FonBotPreferenceActivity.class));
			return true;
		case R.id.localItem:
			startActivity(new Intent(this, FonBotLocalActivity.class));
			return true;
		case R.id.helpItem:
			startActivity(new Intent(this, FonBotHelpActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
