package ro.ieval.fonbot;

import static ro.ieval.fonbot.Utils.toNonNull;
import static ro.ieval.fonbot.Address.Protocol.LOCAL;

import org.eclipse.jdt.annotation.Nullable;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * Activity that lets users send commands from inside the app.
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class FonBotLocalActivity extends Activity {
	/**
	 * BroadcastReceiver that receives command responses. 
	 *
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	private final class ReplyReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(final @Nullable Context context, final @Nullable Intent intent) {
			if (context == null || intent == null) return;
			consoleTextView.setText(consoleTextView.getText() +
					intent.getStringExtra(EXTRA_RESPONSE) + "\n");
		}
	}

	/**
	 * The extra name for the command response.
	 *
	 * @see #RESPONSE_RECEIVED_ACTION
	 */
	public static final String EXTRA_RESPONSE = "response";
	/**
	 * The action for sending back responses to commands.
	 */
	public static final String RESPONSE_RECEIVED_ACTION="ro.ieval.fonbot.LOCAL_RESPONSE";
	/**
	 * Intent filter that catches {@link #RESPONSE_RECEIVED_ACTION}
	 */
	private static final IntentFilter INTENT_FILTER = new IntentFilter(RESPONSE_RECEIVED_ACTION);
	/**
	 * Reply address for local commands
	 */
	private static final Address LOCAL_ADDRESS=new Address(toNonNull(LOCAL), null);
	/**
	 * The one instance of {@link ReplyReceiver} 
	 */
	private final ReplyReceiver replyReceiver = new ReplyReceiver();
	/**
	 * The TextView that contains command responses
	 */
	private TextView consoleTextView;

	@Override
	protected void onCreate(final @Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local);

		consoleTextView = (TextView) findViewById(R.id.consoleTextView);
		final EditText commandEditText = (EditText) findViewById(R.id.argEditText);
		commandEditText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(final @Nullable TextView v, final int actionId, final @Nullable KeyEvent event) {
				if(actionId==EditorInfo.IME_ACTION_SEND||
						actionId==EditorInfo.IME_NULL){
					final String[] commandWithArgs = Utils.shellwords(toNonNull(commandEditText.getText().toString()));
					final String command=commandWithArgs[0];
					final String[] args=new String[commandWithArgs.length-1];
					System.arraycopy(commandWithArgs, 1, args, 0, args.length);
					commandEditText.setText("");
					Utils.processCommand(FonBotLocalActivity.this, toNonNull(command), args,
							toNonNull(LOCAL_ADDRESS));
					return true;
				}
				return false;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		this.registerReceiver(replyReceiver, INTENT_FILTER);
	}

	@Override
	protected void onPause() {
		super.onPause();
		this.unregisterReceiver(replyReceiver);
	}
}