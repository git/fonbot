package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;

import static ro.ieval.fonbot.Utils.toNonNull;

import org.eclipse.jdt.annotation.Nullable;

import ro.ieval.fonbot.Utils.MessageType;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * BroadcastReceiver that receives various broadcasts sent by the sistem
 *
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class ProtectedBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(@Nullable final Context context, @Nullable final Intent intent) {
		if(context==null || intent==null)
			return;

		final String action=intent.getAction();

		if(action.equals(Intent.ACTION_BATTERY_LOW)){
			Utils.sendMessage(context, toNonNull(MessageType.BATTERY), battery_low);
			Heavy.describeBatteryLevel(context, null, toNonNull(MessageType.BATTERY));
		} else if(action.equals(Intent.ACTION_BATTERY_OKAY)){
			Utils.sendMessage(context, toNonNull(MessageType.BATTERY),battery_okay);
			Heavy.describeBatteryLevel(context, null, toNonNull(MessageType.BATTERY));
		} else if(action.equals(Intent.ACTION_BOOT_COMPLETED)){
			Utils.sendMessage(context, toNonNull(MessageType.BOOT), device_booted);
		} else if(action.equals(ConnectivityManager.CONNECTIVITY_ACTION))
			context.startService(new Intent(context, FonBotMainService.class));
	}

}
