package ro.ieval.fonbot;

import static ro.ieval.fonbot.R.string.*;

import static ro.ieval.fonbot.Utils.toNonNull;

import org.eclipse.jdt.annotation.Nullable;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * A dialog created by the {@link Utils.Command#DIALOG DIALOG} command. Includes a message and a list of buttons, all user-defined
 * 
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class DialogActivity extends Activity {
	/** Extra: string array of dialog buttons */
	public static final String EXTRA_BUTTONS = "buttons";
	/** Extra: dialog message */
	public static final String EXTRA_MESSAGE = "message";
	/** Extra: reply Address */
	public static final String EXTRA_REPLYTO = "replyto";

	/** true if the dialog is finishing because the user tapped a button, false otherwise */
	private boolean gotAnswer=false;
	/** Reply address */
	private Address address;

	@Override
	protected void onCreate(@Nullable final Bundle icicle) {
		super.onCreate(icicle);
		address=new Address(toNonNull(getIntent().getStringExtra(EXTRA_REPLYTO)));
		final LinearLayout layout=new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);
		if(getIntent().hasExtra(EXTRA_MESSAGE)){
			final String text=getIntent().getStringExtra(EXTRA_MESSAGE);
			final TextView tv=new TextView(this);
			tv.setText(text);
			layout.addView(tv);
		}
		final String[] buttonLabels=getIntent().getStringArrayExtra(EXTRA_BUTTONS);

		final OnClickListener buttonListener=new OnClickListener() {
			@Override
			public void onClick(@Nullable final View v) {
				if(v==null)
					return;

				Utils.sendMessage(DialogActivity.this, toNonNull(address), 
						user_tapped_fmt, ((Button)v).getText());
				gotAnswer=true;
				finish();
			}
		};
		for(final String label : buttonLabels){
			final Button b=new Button(this);//NOPMD buttons must be different
			b.setText(label);
			b.setOnClickListener(buttonListener);
			layout.addView(b);
		}
		setContentView(layout);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if(!gotAnswer){
			Utils.sendMessage(this, toNonNull(address),
					user_navigated_away_from_dialog);
			gotAnswer=true;
		}
		finish();
	}

	@Override
	public void onBackPressed() {
		if(!gotAnswer){
			Utils.sendMessage(this, toNonNull(address),
					user_canceled_dialog);
			gotAnswer=true;
		}
		finish();
	}
}
