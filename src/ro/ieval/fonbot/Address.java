package ro.ieval.fonbot;

import org.eclipse.jdt.annotation.Nullable;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * An address to which messages can be sent using {@link Utils#sendMessage(android.content.Context, Address, int)}
 * 
 * Addresses are defined by a protocol and a data part. The data is a protocol-dependent opaque String.
 * 
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
final class Address {
	/**
	 * The protocol of an Address.
	 * @author Marius Gavrilescu <marius@ieval.ro>
	 */
	public static enum Protocol{
		/**
		 * The protocol used for sending messages via short text messages. The data for a SMS address is the phone number.
		 */
		SMS,
		/**
		 * The protocol used for sending messages via anything handled by the iEval server. The data is an opaque String supplied by the iEval server.
		 */
		HTTP,
		/**
		 * The protocol used for sending messages to {@link FonBotLocalActivity}
		 */
		LOCAL,
		/**
		 * The protocol used for suppressing messages.
		 */
		NULL
	}

	/**
	 * An address for suppressing messages
	 */
	public static final Address BLACKHOLE=new Address(Utils.toNonNull(Protocol.NULL), null); 
	/**
	 * The protocol part of the Address.
	 */
	public final Protocol protocol;
	/**
	 * The data part of the Address. Can be null
	 */
	public final String data;

	/** The ID of this request. Used in annotations. Can be null */
	public final transient String requestId;

	/**
	 * Construct an Address from its parts
	 * 
	 * @param protocol the protocol part of the Address
	 * @param data the data part of the Address
	 */
	public Address(final Protocol protocol, final @Nullable String data){
		this.protocol=protocol;
		this.data=data;
		this.requestId=null;
	}

	/**
	 * Construct an Address from its parts
	 *
	 * @param protocol the protocol part of the Address
	 * @param data the data part of the Address
	 * @param requestId the request ID
	 */
	public Address(final Protocol protocol, final @Nullable String data, final String requestId){
		this.protocol=protocol;
		this.data=data;
		this.requestId=requestId;
	}

	/**
	 * Construct an Address from its string representation (the protocol and data in this order separated by a single space character). Does the reverse of {@link #toString()}
	 * 
	 * @param address the Address string representation
	 * @throws IllegalArgumentException if the protocol part is not a member of the {@link Protocol} enum
	 */
	public Address(final String address) throws IllegalArgumentException{
		final String[] parts=address.split(" ", 2);
		this.protocol=Protocol.valueOf(parts[0]);
		this.data=parts[1];
		this.requestId=null;
	}

	/**
	 * Returns the string representation of an Address, which is the protocol and data in this order separated by a single space character
	 */
	@Override
	public String toString(){
		return protocol+" "+data;
	}
}
