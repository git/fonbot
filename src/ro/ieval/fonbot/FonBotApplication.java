package ro.ieval.fonbot;

import android.app.Application;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/*
 * Copyright © 2013 Marius Gavrilescu
 * 
 * This file is part of FonBot.
 *
 * FonBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FonBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FonBot.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * The {@link Application} class of FonBot. Registers the device with GCM, starts the TTS service and keeps global state.
 * 
 * @author Marius Gavrilescu <marius@ieval.ro>
 */
public final class FonBotApplication extends Application {
	/**
	 * iEval GCM sender id
	 */
	public static final String GCM_SENDER_ID = "379674287523";

	@Override
	public void onCreate() {
		super.onCreate();

		final TelephonyManager tman=(TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		tman.listen(new FonBotPhoneStateListener(this), PhoneStateListener.LISTEN_CALL_STATE);

		startService(new Intent(this, FonBotMainService.class));

//		Thread.setDefaultUncaughtExceptionHandler(new RemoteCrashdumpHandler(this));
	}
}
